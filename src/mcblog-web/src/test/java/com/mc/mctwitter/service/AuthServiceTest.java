/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;

import com.mc.mctwitter.DatastoreMock;
import com.mc.mctwitter.entity.Session;
import com.mc.mctwitter.entity.User;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author david
 */
public class AuthServiceTest {
    
    private final DatastoreMock helper = new DatastoreMock();
    
    public AuthServiceTest() {
    }
    
    @Before
    public void setUp() {
        helper.setUp();
    }
    
    @After
    public void tearDown() {
        helper.tearDown();
    }

    @Test
    public void testLogin() {
        AuthService svc = new AuthService();
        svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        
        Session ss = svc.login("asd@qwe.hu", "qweqwe");
        Assert.assertNotNull("Login failed!", ss);
    }

    @Test
    public void testLogout() {
        AuthService svc = new AuthService();
        svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        
        Session ss = svc.login("asd@qwe.hu", "qweqwe");
        Assert.assertNotNull("Login failed!", ss);
        
        svc.logout(ss.getSsid());
        
        ss = svc.findSessionById(ss.getSsid());
        Assert.assertNull("Logout failed!", ss);
    }

    @Test
    public void testRegister() {
        AuthService svc = new AuthService();
        User user = svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        Assert.assertNotNull("Register failed!", user);
    }

    @Test
    public void testFindSessionById() {
        AuthService svc = new AuthService();
        svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        
        Session ss = svc.login("asd@qwe.hu", "qweqwe");
        Assert.assertNotNull("Login failed!", ss);
        
        ss = svc.findSessionById(ss.getSsid());
        
        Assert.assertNotNull("Find session by id failed!", ss);
    }

    @Test
    public void testFindUserById() {
        AuthService svc = new AuthService();
        User user = svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        
        user = svc.findUserById(user.getId());
        Assert.assertNotNull("Find user by id failed!", user);
    }

    @Test
    public void testFindUserByEmail() {
        AuthService svc = new AuthService();
        User user = svc.register("Test Pista", "asd@qwe.hu", "qweqwe");
        
        user = svc.findUserByEmail(user.getEmail());
        Assert.assertNotNull("Find user by email failed!", user);
    }
    
}

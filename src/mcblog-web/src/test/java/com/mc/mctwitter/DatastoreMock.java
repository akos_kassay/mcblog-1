/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.googlecode.objectify.ObjectifyService;
import com.googlecode.objectify.util.Closeable;
import com.mc.mctwitter.entity.Blog;
import com.mc.mctwitter.entity.Follow;
import com.mc.mctwitter.entity.Post;
import com.mc.mctwitter.entity.Session;
import com.mc.mctwitter.entity.User;

/**
 *
 * @author david
 */
public class DatastoreMock {
    
    private final LocalServiceTestHelper helper = new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig());
    private Closeable ofySession;
    
    public void setUp() {
        helper.setUp();
        ObjectifyService.register(User.class);
        ObjectifyService.register(Session.class);
        ObjectifyService.register(Blog.class);
        ObjectifyService.register(Post.class);
        ObjectifyService.register(Follow.class);
        ofySession = ObjectifyService.begin();
    }
    
    public void tearDown() {
        ofySession.close();
        helper.tearDown();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc;

import java.util.Map;
import lombok.Getter;

/**
 *
 * @author david
 */
public class ApiException extends Exception {
    
    @Getter
    private final int statusCode;
    
    @Getter
    private final Map<String, String> fieldErrors;
    
    public ApiException(String message) {
        this(500, message, null);
    }
    
    public ApiException(Map<String, String> fieldErrors) {
        this(400, null, fieldErrors);
    }
    
    public ApiException(int statusCode, Map<String, String> fieldErrors) {
        this(statusCode, null, fieldErrors);
    }

    public ApiException(int statusCode, String message) {
        this(statusCode, message, null);
    }
    
    public ApiException(int statusCode, String message, Map<String, String> fieldErrors) {
        super(message);
        this.statusCode = statusCode;
        this.fieldErrors = fieldErrors;
    }
    
    
}

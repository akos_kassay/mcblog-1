/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mc.mctwitter.entity.Blog;
import com.mc.mctwitter.entity.Follow;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author david
 */
public class BlogService {
    
    private final FollowService followSvc = new FollowService();
    
    public BlogService() {
    }

    public Blog createBlog(long userId, String name, String content) {
        name = name.trim();
        content = content.trim();
        
        Blog blog = new Blog(null, name, content, userId, System.currentTimeMillis());
        ObjectifyService.ofy().save().entity(blog);
        
        return blog;
    }

    public Blog findBlogById(long blogId) {
        return ObjectifyService.ofy().load().now(Key.create(Blog.class, blogId));
    }

    public List<Blog> listOwnBlogs(long userId) {
        return ObjectifyService.ofy().load().type(Blog.class).filter("userId", userId).list();
    }

    public List<Blog> listFollowedBlogs(long userId, int offset, int limit) {
        List<Follow> follows = followSvc.listUserFollows(userId);
        
        List<Long> blogIds = new ArrayList<>();
        for (Follow follow : follows) {
            blogIds.add(follow.getBlogId());
        }
        
        List<Key<Blog>> blogKeys = new ArrayList<>();
        for (long blogId : blogIds) {
            blogKeys.add(Key.create(Blog.class, blogId));
        }
        
        Map<Key<Blog>, Blog> resMap = ObjectifyService.ofy().load().keys(blogKeys);
        
        List<Blog> res = new ArrayList<>();
        res.addAll(resMap.values());
        
        return res;
    }
    
}

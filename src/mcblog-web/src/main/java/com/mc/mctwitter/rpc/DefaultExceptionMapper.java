/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc;

import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 *
 * @author david
 */
@Provider
public class DefaultExceptionMapper implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        int code = 500;
        String msg = "Server error occured!";
        Map<String, String> fieldErrors = null;
        if (e instanceof ApiException) {
            ApiException ex = (ApiException) e;
            code = ex.getStatusCode();
            msg = ex.getMessage();
            fieldErrors = ex.getFieldErrors();
        } else if (e instanceof AccessDeniedException) {
            AccessDeniedException ex = (AccessDeniedException) e;
            code = 401;
            msg = "Unauthorized!";
        } else {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, e.getMessage(), e);
        }
        StringBuilder sb = new StringBuilder();
        sb.append("{\"error\": true, \"code\": ").append(code);
        
        if (msg != null) {
            sb.append(", \"message\": \"").append(msg).append("\"");
        } else {
            sb.append(", \"message\": null");
        }

        if (fieldErrors != null && fieldErrors.size() > 0) {
            sb.append(",\n\"fieldErrors\": {\n");
            boolean first = true;
            for (Map.Entry<String, String> entry : fieldErrors.entrySet()) {
                if (!first) {
                    sb.append(",");
                    sb.append("\n");
                }
                first = false;
                sb.append("\"").append(entry.getKey()).append("\":\"").append(entry.getValue()).append("\""); 
            }
            sb.append("\n}");
        }
        
        sb.append("}");

        return Response
                .status(Response.Status.fromStatusCode(code))
                .type(MediaType.APPLICATION_JSON_TYPE)
                .entity(sb.toString())
                .build();
    }

}

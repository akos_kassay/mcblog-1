/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;

import com.googlecode.objectify.ObjectifyService;
import com.mc.mctwitter.entity.Follow;
import java.util.List;

/**
 *
 * @author david
 */
public class FollowService {
    
    public FollowService() {
    }

    public Follow followBlog(long blogId, long userId) {
        Follow follow = ObjectifyService.ofy().load().type(Follow.class).filter("blogId", blogId).filter("userId", userId).first().now();
        if (follow != null) {
            return follow;
        }
        
        follow = new Follow(null, blogId, userId, System.currentTimeMillis());
        ObjectifyService.ofy().save().entity(follow).now();
        return follow;
    }

    public void unfollowBlog(long blogId, long userId) {
        Follow f = ObjectifyService.ofy().load().type(Follow.class).filter("blogId", blogId).filter("userId", userId).first().now();
        if (f == null) {
            return;
        }
        
        ObjectifyService.ofy().delete().entity(f).now();
    }

    public int getFollowCount(long blogId) {
        return ObjectifyService.ofy().load().type(Follow.class).filter("blogId", blogId).count();
    }

    public List<Follow> listUserFollows(long userId) {
        return ObjectifyService.ofy().load().type(Follow.class).filter("userId", userId).list();
    }
    
}

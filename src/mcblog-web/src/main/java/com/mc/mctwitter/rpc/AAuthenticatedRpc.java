/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc;

import com.mc.mctwitter.entity.Session;
import com.mc.mctwitter.service.AuthService;

/**
 *
 * @author david
 */
public abstract class AAuthenticatedRpc {
    
    private AuthService authSvc = new AuthService();
    
    public Session authenticate(String ssid) {
        Session ss = authSvc.findSessionById(ssid);
        if (ss == null) {
            throw new AccessDeniedException();
        }
        
        return ss;
    }
    
}

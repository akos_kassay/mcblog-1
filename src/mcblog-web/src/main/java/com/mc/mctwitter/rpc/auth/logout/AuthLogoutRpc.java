/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.rpc.auth.logout;

import com.mc.mctwitter.entity.Session;
import com.mc.mctwitter.rpc.AAuthenticatedRpc;
import com.mc.mctwitter.service.AuthService;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
 *
 * @author david
 */
@Path("/auth")
@Consumes(value = "application/json")
@Produces(value = "application/json")
public class AuthLogoutRpc extends AAuthenticatedRpc {
    
    private AuthService authSvc = new AuthService();
    
    @POST
    @Path("/logout")
    public void logout(@HeaderParam(value = "ssid") String ssid) {
        Session ss = authenticate(ssid);
        authSvc.logout(ss.getSsid());
    }
    
}

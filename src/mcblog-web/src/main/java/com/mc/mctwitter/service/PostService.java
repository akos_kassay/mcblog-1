/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mc.mctwitter.entity.Blog;
import com.mc.mctwitter.entity.Follow;
import com.mc.mctwitter.entity.Post;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author david
 */
public class PostService {
    
    private final FollowService followSvc = new FollowService();
    private final BlogService blogSvc = new BlogService();
    
    public PostService() {
    }

    public Post findPostById(long postId) {
        return ObjectifyService.ofy().load().now(Key.create(Post.class, postId));
    }

    public Post createPost(long userId, long blogId, String title, String content) {
        Blog blog = blogSvc.findBlogById(blogId);
        if (blog == null) {
            throw new IllegalArgumentException("Can't find blog with id: " + blogId);
        }
        
        if (blog.getOwnerId() != userId) {
            throw new IllegalArgumentException("User: " + userId + " isn't the owner of the blog: " + blogId);
        }
        
        Post post = new Post(null, title, content, blogId, userId);
        ObjectifyService.ofy().save().entity(post).now();
        
        return post;
    }

    public List<Post> listBlogPosts(long blogId, int offset, int limit) {
        return ObjectifyService.ofy().load().type(Post.class).filter("blogId", blogId).offset(offset).limit(limit).list();
    }

    public List<Post> listPosts(int offset, int limit) {
        return ObjectifyService.ofy().load().type(Post.class).offset(offset).limit(limit).list();
    }

    public List<Post> listFollowedPosts(long userId, int offset, int limit) {
        List<Follow> follows = followSvc.listUserFollows(userId);
        List<Long> followedBlogIds = new ArrayList<>();
        for (Follow f : follows) {
            followedBlogIds.add(f.getBlogId());
        }
        
        if (followedBlogIds.size() < 1) {
            return new ArrayList<>();
        }
        
        return ObjectifyService.ofy().load().type(Post.class).filter("blogId", followedBlogIds).list();
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mc.mctwitter.service;

import com.googlecode.objectify.Key;
import com.googlecode.objectify.ObjectifyService;
import com.mc.mctwitter.entity.Session;
import com.mc.mctwitter.entity.User;
import java.util.UUID;

/**
 *
 * @author david
 */
public class AuthService {
    
    public AuthService() {
    }

    public Session login(String email, String password) {
        email = email.trim().toLowerCase();
        password = password.trim();
        
        User user = ObjectifyService.ofy().load().type(User.class).filter("email", email).first().now();
        if (user == null) {
            return null;
        }
        
        if (!user.getPassword().equals(password)) {
            return null;
        }
        
        Session ss = new Session(UUID.randomUUID().toString(), user.getId(), System.currentTimeMillis());
        ObjectifyService.ofy().save().entity(ss).now();
        
        return ss;
    }

    public void logout(String ssid) {
        ObjectifyService.ofy().delete().key(Key.create(Session.class, ssid)).now();
    }

    public User register(String name, String email, String password) {
        if (name == null) {
            throw new NullPointerException();
        }
        
        name = name.trim();
        if (name.length() < 3) {
            throw new IllegalArgumentException("Minimum name length: 3");
        }
        
        if (email == null) {
            throw new NullPointerException();
        }
        
        email = email.trim().toLowerCase();
        if (email.length() < 5) {
            throw new IllegalArgumentException("Invalid email address");
        }
        
        if (password == null) {
            throw new NullPointerException();
        }
        
        password = password.trim();
        if (password.length() < 6) {
            throw new IllegalArgumentException("Minimum password length: 6");
        }
        
        User user = ObjectifyService.ofy().load().type(User.class).filter("email", email).first().now();
        if (user != null) {
            throw new IllegalArgumentException("Email address already exists.");
        }
        
        user = User.builder()
                .email(email)
                .password(password)
                .name(name)
                .build();
        
        ObjectifyService.ofy().save().entity(user).now();
        
        return user;
    }

    public Session findSessionById(String ssid) {
        return ObjectifyService.ofy().load().now(Key.create(Session.class, ssid));
    }

    public User findUserById(long userId) {
        return ObjectifyService.ofy().load().now(Key.create(User.class, userId));
    }
    
    public User findUserByEmail(String email) {
        email = email.trim().toLowerCase();
        return ObjectifyService.ofy().load().type(User.class).filter("email", email).first().now();
    }
    
}
